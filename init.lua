---@diagnostic disable-next-line: undefined-global
local vim = vim

local on_attach = function(_, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)
end

require('wilvim').startup {
    nvimTreeConfig = nil,
    bufferlineConfig = nil,
    packages = nil,
    advancedMode = true,
    treeSitterConfig = nil,
    lspOnAttach = on_attach,
}

vim.opt.linebreak = true
vim.opt.textwidth=100
vim.opt.ruler = true
vim.opt.cursorline = true
vim.opt.autochdir = true
vim.opt.undolevels=1000


local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<leader>k', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', '<leader>j', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)

vim.api.nvim_set_hl(0, "Pmenu", { bg = "gray" })

print 'Hello in Vim-nya~ >^.^<'
