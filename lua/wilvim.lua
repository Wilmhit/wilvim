---@diagnostic disable-next-line: undefined-global
local vim = vim

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.g.mapleader = ' '

vim.opt.clipboard='unnamedplus'
vim.opt.termguicolors = true
vim.opt.matchtime=2
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.wrap = false
vim.opt.hlsearch = true
vim.opt.smartcase = true
vim.opt.ignorecase = true
vim.opt.incsearch = true
vim.opt.autoindent = true
vim.opt.expandtab = true
vim.opt.showmatch  = true
vim.opt.smartindent = true

vim.api.nvim_set_keymap('n', '<leader>vv', ':vsplit $MYVIMRC<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>vs', ':source $MYVIMRC<cr>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>e', ':NvimTreeFindFileToggle<cr>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'L', ':BufferLineCycleNext<cr>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'H', ':BufferLineCyclePrev<cr>', {noremap = true, silent = true})

local function create_autocmds()
    local setftgroup = vim.api.nvim_create_augroup("filetypedetect", {})

    vim.api.nvim_create_autocmd( 'TextYankPost', {
        pattern = "*",
        command = "silent! lua vim.highlight.on_yank()"
    })

    vim.api.nvim_create_autocmd( {'BufNewFile','BufRead'}, {
        pattern = "*.yaml",
        command = "if search('{{.\\+}}', 'nw') | setlocal filetype=gotmpl | endif",
        group = setftgroup,
    })
end
create_autocmds()

local function advancedModeConfig(config)
    local hasTreeSitter, tsConfig = pcall(require, 'nvim-treesitter.configs')
    local hasMason, mason = pcall(require, 'mason')
    local hasMasonLspConfig, masonLspConfig = pcall(require, 'mason-lspconfig')
    local hasLspConfig, _ = pcall(require, 'lspconfig')
    local hasCoq, _ = pcall(require, 'coq')

    if type(config.treeSitterConfig) == "nil" then
        config.treeSitterConfig = {
            auto_install = true,
            ensure_installed = {
                "c",
                "rust",
                "lua",
                "vim",
                "help",
                "javascript",
                "python",
            },
            highlight = {
                enable = true,
            },
            indent = {
                enable = true
            }
        }
    end

    if type(config.masonLspConfig) == "nil" then
        config.masonLspConfig = {
            ensure_installed = {
                "clangd",
                "pyright",
                "lua_ls"
            }
        }
    end

    if type(config.masonConfig) == "nil" then
        config.masonConfig = {}
    end

    if
        not hasTreeSitter or
        not hasMason or
        not hasMasonLspConfig or
        not hasLspConfig or
        not hasCoq -- it must have coq
    then
        vim.cmd 'echo "You need to do :PackerInstall after enabling advancedMode"'
        return
    end

    tsConfig.setup(config.treeSitterConfig)
    mason.setup(config.masonConfig)
    masonLspConfig.setup(config.masonLspConfig)
    masonLspConfig.setup_handlers {
        function (server_name)
            local lsp = require("lspconfig")
            local coq = require("coq")
            lsp[server_name].setup(coq.lsp_ensure_capabilities({
                lsp_flags = config.lspFlags,
                on_attach = config.lspOnAttach
            }))
        end,
    }

    vim.g.coq_settings = {
        auto_start = "shut-up",
        xdg = true,
        clients = {
            snippets = {
                warn = {}
            }
        }
    }

    -- Helm
    local parser_config = require'nvim-treesitter.parsers'.get_parser_configs()
    parser_config.gotmpl = {
      install_info = {
        url = "https://github.com/ngalaiko/tree-sitter-go-template",
        files = {"src/parser.c"}
      },
      filetype = "gotmpl",
      used_by = {"gohtmltmpl", "gotexttmpl", "gotmpl", "yaml"}
    }

    vim.cmd 'COQnow --shut-up'
end

local function advancedModeInstallation(use)
    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
            ts_update()
        end,
    }
    use {
        'williamboman/mason.nvim',
        'williamboman/mason-lspconfig.nvim',
        'neovim/nvim-lspconfig',
    }
    use {'ms-jpq/coq_nvim', branch='coq'}
end

local function basicModeInstallation(use)
            use 'wbthomason/packer.nvim'
            use {
                'nvim-tree/nvim-tree.lua',
                requires = {
                    'nvim-tree/nvim-web-devicons',
                }
            }
            use 'lambdalisue/suda.vim'
            use {'akinsho/bufferline.nvim', tag = "v3.*", requires = 'nvim-tree/nvim-web-devicons'}
            use 'tpope/vim-commentary'
end

local function basicModeConfig(config)
        local hasBufferline, bufferline = pcall(require, 'bufferline')
        local hasNvimTree, nvimTree = pcall(require, 'nvim-tree')

        if not hasBufferline or not hasNvimTree then
            vim.cmd 'echo "Please install Packer for NeoVim and run :PackerInstall"'
            return
        end

        if type(config.nvimTreeConfig) == "nil" then
            config.nvimTreeConfig = {
                tab = { sync = { open = true, close = true } },
                git = { ignore = false },
                view = { adaptive_size = true },
                renderer = { indent_markers = { enable = true } }
            }
        end

        if type(config.bufferlineConfig) == "nil" then
            config.bufferlineConfig = {
                options = {
                    mode = 'tabs',
                    show_close_icon = false,
                    show_buffer_close_icons = true,
                    diagnostics = 'nvim_lsp',
                    offsets = {
                        {
                            filetype = 'NvimTree',
                            text = "Explorer",
                        }
                    }
                }
            }
        end

        nvimTree.setup(config.nvimTreeConfig)
        bufferline.setup(config.bufferlineConfig)
end

return {
    startup = function(config)
        basicModeConfig(config)

        if config.advancedMode == true then
            advancedModeConfig(config)
        end

        require('packer').startup(function(use)
            basicModeInstallation(use)

            if config.advancedMode == true then
                advancedModeInstallation(use)
            end

            if type(config.packages) == nil then
                config.packages(use)
            end
        end)

    end
}
